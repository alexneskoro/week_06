package ru.edu;


import java.sql.DriverManager;


public final class LocalDbCreation {

    /** Data base URL.
    *
    */
    public static final String JDBC_SQLITE_LOCAL_DB =
            "jdbc:sqlite:./LocalDB/Students.db";


    /** Constructor.
     *
     */
    private LocalDbCreation() {

    }

    /** Utility for creating connection.
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {

        DriverManager.getConnection(JDBC_SQLITE_LOCAL_DB, "sa", "");

    }

}
