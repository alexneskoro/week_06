package ru.edu.model;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей.
 */
public final class Student {

    /**
     * Первичный ключ.
     *
     * Рекомендуется генерировать его только
     * внутри StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект не будет сохранен в БД
     * , он не должен иметь значение id.
     */
    private UUID id;

    /** Firstname.
     *
     */
    private String firstName;

    /** Lastname.
     *
     */
    private String lastName;

    /** BirthDate.
     *
     */
    private LocalDate birthDate;

    /** Flag if student is graduated.
     *
     */
    private boolean isGraduated;

    /** Constructor.
     *
     */
    private Student() {

    }

    /** Builder.
     *
     * @return Builder
     */
    public static Builder builder() {
        return new Student().new Builder();
    }

    public final class Builder {

        /** Constructor.
         *
         */
        private Builder() {

        }

        /** Setting id.
         *
         * @param pId
         * @return Builder
         */
        public Builder setId(final UUID pId) {
            Student.this.id = pId;
            return this;
        }

        /** Setting first name.
         *
         * @param pFirstName
         * @return Builder
         */
        public Builder setFirstName(final String pFirstName) {
            Student.this.firstName = pFirstName;
            return this;
        }

        /** Setting last name.
         *
         * @param pLastName
         * @return Builder
         */
        public Builder setLastName(final String pLastName) {
            Student.this.lastName = pLastName;
            return this;
        }

        /** Setting date of birth.
         *
         * @param pBirthDate
         * @return Builder
         */
        public Builder setBirthDate(final LocalDate pBirthDate) {
            Student.this.birthDate = pBirthDate;
            return this;
        }

        /** Setting graduating flag.
         *
         * @param pIsGraduated
         * @return Builder
         */
        public Builder setIsGraduated(final boolean pIsGraduated) {
            Student.this.isGraduated = pIsGraduated;
            return this;
        }

        /** Build student.
         *
         * @return Student
         */
        public Student build() {
            return Student.this;
        }

    }

    /** Set first name.
     *
     * @param pFirstName
     */
    public void setFirstName(final String pFirstName) {
        this.firstName = pFirstName;
    }

    /** Set first name.
     *
     * @param pLastName
     */
    public void setLastName(final String pLastName) {
        this.lastName = pLastName;
    }

    /** Set date of birth.
     *
     * @param pBirthDate
     */
    public void setBirthDate(final LocalDate pBirthDate) {
        this.birthDate = pBirthDate;
    }

    /** Set graduating flag.
     *
     * @param pGraduated
     */
    public void setGraduated(final boolean pGraduated) {
        isGraduated = pGraduated;
    }

    /** Getting id.
     *
     * @return UUID
     */
    public UUID getId() {
        return id;
    }

    /** Getting first name.
     *
     * @return String
     */
    public String getFirstName() {
        return firstName;
    }

    /** Getting last name.
     *
     * @return String
     */
    public String getLastName() {
        return lastName;
    }


    /** Getting date of birth.
     *
     * @return String
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }


    /** Getting graduating flag.
     *
     * @return boolean
     */
    public boolean isGraduated() {
        return isGraduated;
    }

}
