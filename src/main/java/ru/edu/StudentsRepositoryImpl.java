package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class StudentsRepositoryImpl implements StudentsRepositoryCRUD {
    /** Insert student query.
     *
     */
    private static final String INSERT_STUDENT = "INSERT INTO Students "
            + "(id, first_name, last_name, birth_date, is_graduated) "
            + "VALUES (?, ?, ?, ?, ?);";

    /** Update student query.
     *
     */
    private static final String UPDATE_STUDENT = "UPDATE Students SET"
            + " first_name = ?, "
            + "last_name = ?, "
            + "birth_date = ?, "
            + "is_graduated = ?"
            + " WHERE id = ?;";

    /** Delete student by list of ids query.
     *
     */
    private static final String DELETE_STUDENT = "DELETE FROM Students "
            + " WHERE id in (";

    /** select student by id student query.
     *
     */
    private static final String SELECT_BY_ID = "SELECT id, first_name"
            + ", last_name, birth_date, is_graduated "
            + "FROM Students WHERE id = ?;";

    /** Select all students query.
     *
     */
    private static final String SELECT_ALL_STUDENTS = "SELECT * FROM Students;";


    /** Connection with DB.
     *
     */
    private Connection connection;


    /** Constructor.
     *
     * @param pConnection
     */
    public StudentsRepositoryImpl(final Connection pConnection) {
        this.connection = pConnection;
    }


    private int executeUpdate(final String sql, final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Student> executeQuery(final String sql,
                                       final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            List<Student> resultList = new ArrayList<>();
            final ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                resultList.add(getStudent(resultSet));
            }

            return resultList;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Student getStudent(final ResultSet resultSet) {
        try {
            return Student.builder()
                    .setId(UUID.fromString(resultSet.getString("id")))
                    .setFirstName(resultSet.getString("first_name"))
                    .setLastName(resultSet.getString("last_name"))
                    .setBirthDate(LocalDate.parse(
                            resultSet.getString("birth_date"))
                    )
                    .setIsGraduated(resultSet.getBoolean("is_graduated"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        if (student.getId() != null) {
            throw new IllegalArgumentException("Students id is not null!");
        }
        UUID id = UUID.randomUUID();

        try {
            int insertedRows = executeUpdate(INSERT_STUDENT,
                    id.toString(),
                    student.getFirstName(),
                    student.getLastName(),
                    student.getBirthDate(),
                    student.isGraduated());
            if (insertedRows == 0) {
                throw new IllegalStateException("No students was inserted!");
            }
            return id;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return
     */
    @Override
    public Student selectById(final UUID id) {
        try {
            List<Student> resultList = executeQuery(SELECT_BY_ID,
                    id.toString());

            if (resultList.size() == 0) {
                throw new IllegalStateException(
                        "NO students was found with such id!");
            }

            return resultList.get(0);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        try {
            List<Student> resultList = executeQuery(SELECT_ALL_STUDENTS);

            if (resultList.size() == 0) {
                throw new IllegalStateException(
                        "NO students was found with such id!");
            }

            return resultList;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        if (student.getId() == null) {
            throw new IllegalArgumentException("Student have no id!");
        }
        try {
            int resultUpdateCount = executeUpdate(UPDATE_STUDENT,
                    student.getFirstName(),
                    student.getLastName(),
                    student.getBirthDate(),
                    student.isGraduated(),
                    student.getId());

            if (resultUpdateCount == 0) {
                throw new IllegalStateException("No student was updated!");
            }

            return resultUpdateCount;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        int resultCount = 0;

        if (idList.size() == 0) {
            return 0;
        }

        StringBuffer deleteSQL = new StringBuffer(DELETE_STUDENT);

        for (int i = 0; i < idList.size() - 1; i++) {
            deleteSQL.append("?, ");
        }
        deleteSQL.append("?);");

        try {
            int resultDeleteCount = executeUpdate(deleteSQL.toString(),
                    idList.toArray());

            if (resultDeleteCount == 0) {
                throw new IllegalStateException("No student was deleted!");
            }

            return resultDeleteCount;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
