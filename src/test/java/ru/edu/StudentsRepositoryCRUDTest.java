package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static ru.edu.LocalDbCreation.JDBC_SQLITE_LOCAL_DB;

public class StudentsRepositoryCRUDTest {

    String[][] studentsData = { {"ivan", "ivanov", "2001-02-15", "false"}
            ,{"Petr", "Sidorov", "2001-03-01", "false"}
            ,{"Marine", "Magikan", "1991-01-23", "true"}
    };


    @Test
    public void testStudentsRepository() throws SQLException {

        Connection connection = DriverManager.getConnection(JDBC_SQLITE_LOCAL_DB);
        StudentsRepositoryImpl repository = new StudentsRepositoryImpl(connection);


        Student[] student = new Student[studentsData.length];

        for (int i = 0; i < studentsData.length; i++) {
            Student studentForCreating = Student.builder()
                    .setFirstName(studentsData[i][0])
                    .setLastName(studentsData[i][1])
                    .setBirthDate(LocalDate.parse(studentsData[i][2]))
                    .setIsGraduated(Boolean.getBoolean(studentsData[i][3]))
                    .build();

            UUID idOfCreatedStudent = repository.create(studentForCreating);
            student[i] = repository.selectById(idOfCreatedStudent);
            assertEquals(studentsData[i][0], student[i].getFirstName());
            assertEquals(studentsData[i][1], student[i].getLastName());
            assertEquals(studentsData[i][2], student[i].getBirthDate().toString());
            assertEquals(Boolean.getBoolean(studentsData[i][3]), student[i].isGraduated());

        }

        List<Student> studentsList = repository.selectAll();

        for (int i = 0; i < studentsData.length; i++) {
            assertEquals(studentsData[i][0], studentsList.get(i).getFirstName());
            assertEquals(studentsData[i][1], studentsList.get(i).getLastName());
            assertEquals(studentsData[i][2], studentsList.get(i).getBirthDate().toString());
            assertEquals(Boolean.getBoolean(studentsData[i][3]), studentsList.get(i).isGraduated());
        }

        student[0].setFirstName("Ivan");
        student[0].setLastName("Ivanov");
        student[0].setBirthDate(LocalDate.of(2000, 02, 15));
        student[0].setGraduated(true);

        repository.update(student[0]);

        assertEquals("Ivan", student[0].getFirstName());
        assertEquals("Ivanov", student[0].getLastName());
        assertEquals("2000-02-15", student[0].getBirthDate().toString());
        assertTrue(student[0].isGraduated());

        assertEquals(0, repository.remove(new ArrayList<>()));

        List<UUID> idList = new ArrayList<>();
        for (int i = 0; i < student.length; i++) {
            idList.add(student[i].getId());
        }

        assertEquals(student.length, repository.remove(idList));
    }

    @Test(expected = IllegalStateException.class)
    public void removeSQLEx() {
        List<UUID> idList = new ArrayList<>();
        idList.add(UUID.randomUUID());
        new StudentsRepositoryImpl(null).remove(idList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullStudentId() {
        new StudentsRepositoryImpl(null).update(Student.builder().build());
    }

    @Test(expected = IllegalStateException.class)
    public void updateSQLEx() {
        new StudentsRepositoryImpl(null).update(Student.builder().setId(UUID.randomUUID()).build());
    }

    @Test(expected = IllegalStateException.class)
    public void selectAllSQLEx() {
        new StudentsRepositoryImpl(null).selectAll();
    }

    @Test(expected = IllegalStateException.class)
    public void selectByIdNoSUchStudentLEx() throws SQLException {
        Connection connection = DriverManager.getConnection(JDBC_SQLITE_LOCAL_DB);
        new StudentsRepositoryImpl(connection).selectById(UUID.randomUUID());
    }

    @Test(expected = IllegalStateException.class)
    public void selectByIdSQLEx(){
        new StudentsRepositoryImpl(null).selectById(UUID.randomUUID());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNotNullIdEx() throws SQLException {
        Connection connection = DriverManager.getConnection(JDBC_SQLITE_LOCAL_DB);
        new StudentsRepositoryImpl(connection).create(Student.builder().setId(UUID.randomUUID()).build());
    }

    @Test(expected = IllegalStateException.class)
    public void createSQLEx(){
        new StudentsRepositoryImpl(null).create(Student.builder().build());
    }
}